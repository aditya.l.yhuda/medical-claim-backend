﻿using AutoMapper;
using MedicalClaimAPI.DTOs;
using MedicalClaimAPI.Models;

namespace MedicalClaimAPI.MappingProfiles
{
    public class MedicalClaimProfile : Profile
    {
        public MedicalClaimProfile()
        {
            CreateMap<MedicalClaim, MedicalClaimDTO>().ReverseMap();
        }
    }
}
