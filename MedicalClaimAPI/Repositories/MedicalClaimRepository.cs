﻿using MedicalClaimAPI.Data;
using MedicalClaimAPI.Interfaces;
using MedicalClaimAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace MedicalClaimAPI.Repositories
{
    public class MedicalClaimRepository : IMedicalClaimRepository
    {
        private readonly MedicalClaimsDbContext _context;
        public MedicalClaimRepository(MedicalClaimsDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<MedicalClaim>> GetAllAsync()
        {
            return await _context.MedicalClaims.ToListAsync();
        }

        public async Task<MedicalClaim> GetByIdAsync(int id)
        {
            return await _context.MedicalClaims.FindAsync(id);
        }

        public async Task AddAsync(MedicalClaim medicalClaim)
        {
            _context.MedicalClaims.Add(medicalClaim);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(MedicalClaim medicalClaim)
        {
            _context.Entry(medicalClaim).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var medicalClaim = await _context.MedicalClaims.FindAsync(id);
            if (medicalClaim != null)
            {
                _context.MedicalClaims.Remove(medicalClaim);
                await _context.SaveChangesAsync();
            }
        }
    }
}
