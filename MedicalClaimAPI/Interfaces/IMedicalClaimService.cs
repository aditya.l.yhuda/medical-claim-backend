﻿using MedicalClaimAPI.DTOs;

namespace MedicalClaimAPI.Interfaces
{
    public interface IMedicalClaimService
    {
        Task<IEnumerable<MedicalClaimDTO>> GetAllClaimsAsync();
        Task<MedicalClaimDTO> GetClaimByIdAsync(int id);
        Task AddClaimAsync(MedicalClaimDTO medicalClaimDto);
        Task UpdateClaimAsync(MedicalClaimDTO medicalClaimDto);
        Task DeleteClaimAsync(int id);
    }
}
