﻿using MedicalClaimAPI.Models;

namespace MedicalClaimAPI.Interfaces
{
    public interface IMedicalClaimRepository
    {
        Task<IEnumerable<MedicalClaim>> GetAllAsync();
        Task<MedicalClaim> GetByIdAsync(int id);
        Task AddAsync(MedicalClaim medicalClaim);
        Task UpdateAsync(MedicalClaim medicalClaim);
        Task DeleteAsync(int id);
    }
}
