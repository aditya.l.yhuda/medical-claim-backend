﻿using MedicalClaimAPI.DTOs;
using MedicalClaimAPI.DTOs.Response;
using MedicalClaimAPI.GlobalConstants;
using MedicalClaimAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace MedicalClaimAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicalClaimsController : ControllerBase
    {
        private readonly IMedicalClaimService _service;

        public MedicalClaimsController(IMedicalClaimService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<MedicalClaimDTO>>> GetMedicalClaims()
        {
            ResponseType type = ResponseType.Success;
            try
            {
                var claims = await _service.GetAllClaimsAsync();
                if (!claims.Any())
                {
                    type = ResponseType.NotFound;
                }
                return Ok(ResponseHandler.GetAppResponse(type, claims));
            }
            catch (Exception ex)
            {
                return BadRequest(ResponseHandler.GetExceptionResponse(ex));

            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MedicalClaimDTO>> GetMedicalClaim(int id)
        {
            ResponseType type = ResponseType.Success;
            try
            {
                var claim = await _service.GetClaimByIdAsync(id);
                if (claim == null)
                {
                    type = ResponseType.NotFound;
                }
                return Ok(ResponseHandler.GetAppResponse(type, claim));
            }
            catch (Exception ex)
            {
                return BadRequest(ResponseHandler.GetExceptionResponse(ex));

            }
        }

        [HttpPost]
        public async Task<ActionResult<MedicalClaimDTO>> PostMedicalClaim(MedicalClaimDTO medicalClaimDto)
        {
            ResponseType type = ResponseType.Success;

            try
            {
                medicalClaimDto.DateOfService = medicalClaimDto.DateOfService.ToUniversalTime();
                await _service.AddClaimAsync(medicalClaimDto);
                return Ok(ResponseHandler.GetAppResponse(type, medicalClaimDto));
            }
            catch (Exception ex)
            {
                return BadRequest(ResponseHandler.GetExceptionResponse(ex));
            }
            
            //return CreatedAtAction("GetMedicalClaim", new { id = medicalClaimDto.Id }, medicalClaimDto);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicalClaim(int id, MedicalClaimDTO medicalClaimDto)
        {
            if (id != medicalClaimDto.Id)
            {
                return BadRequest();
            }

            await _service.UpdateClaimAsync(medicalClaimDto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedicalClaim(int id)
        {
            await _service.DeleteClaimAsync(id);
            return NoContent();
        }
    }
}
