﻿using AutoMapper;
using MedicalClaimAPI.DTOs;
using MedicalClaimAPI.Interfaces;
using MedicalClaimAPI.Models;

namespace MedicalClaimAPI.Services
{
    public class MedicalClaimService : IMedicalClaimService
    {
        private readonly IMedicalClaimRepository _repository;
        private readonly IMapper _mapper;

        public MedicalClaimService(IMedicalClaimRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<MedicalClaimDTO>> GetAllClaimsAsync()
        {
            var claims = await _repository.GetAllAsync();
            return _mapper.Map<IEnumerable<MedicalClaimDTO>>(claims);
        }

        public async Task<MedicalClaimDTO> GetClaimByIdAsync(int id)
        {
            var claim = await _repository.GetByIdAsync(id);
            return _mapper.Map<MedicalClaimDTO>(claim);
        }

        public async Task AddClaimAsync(MedicalClaimDTO medicalClaimDto)
        {
            var claim = _mapper.Map<MedicalClaim>(medicalClaimDto);
            await _repository.AddAsync(claim);
        }

        public async Task UpdateClaimAsync(MedicalClaimDTO medicalClaimDto)
        {
            var claim = _mapper.Map<MedicalClaim>(medicalClaimDto);
            await _repository.UpdateAsync(claim);
        }

        public async Task DeleteClaimAsync(int id)
        {
            await _repository.DeleteAsync(id);
        }
    }
}
