﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MedicalClaimAPI.Models
{
    [Table("medical_claim")]
    public class MedicalClaim
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("patien_name")]
        [MaxLength(100)]
        public string PatientName { get; set; }

        [Column("date_of_service")]
        public DateTime DateOfService { get; set; }

        [Column("medical_provider")]
        [MaxLength(150)]
        public string MedicalProvider { get; set; }

        [Column("diagnosis")]
        [MaxLength(500)]
        public string Diagnosis { get; set; }

        [Column("claim_amount")]
        public decimal ClaimAmount { get; set; }

        [Column("status")]
        [MaxLength(20)]
        public string Status { get; set; }
    }
}
