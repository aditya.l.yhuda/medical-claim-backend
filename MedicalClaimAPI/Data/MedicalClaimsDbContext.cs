﻿using MedicalClaimAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace MedicalClaimAPI.Data
{
    public class MedicalClaimsDbContext : DbContext
    {
        public MedicalClaimsDbContext() { }
        public MedicalClaimsDbContext(DbContextOptions<MedicalClaimsDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<MedicalClaim> MedicalClaims { get; set; }
    }
}
