﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MedicalClaimAPI.DTOs
{
    public class MedicalClaimDTO
    {
        public int Id { get; set; }
        [Required]
        public string PatientName { get; set; }
        [Required]
        public DateTime DateOfService { get; set; }
        [Required]
        public string MedicalProvider { get; set; }
        public string Diagnosis { get; set; }
        public decimal ClaimAmount { get; set; }
        [Required]
        public string Status { get; set; }
    }
}
