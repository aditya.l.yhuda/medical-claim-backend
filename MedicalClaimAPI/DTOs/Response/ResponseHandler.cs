﻿using MedicalClaimAPI.GlobalConstants;

namespace MedicalClaimAPI.DTOs.Response
{
    public class ResponseHandler
    {
        public static ApiResponse GetExceptionResponse(Exception ex)
        {
            ApiResponse response = new ApiResponse();
            response.status = 1;
            response.data = ex.Message;
            return response;
        }
        public static ApiResponse GetAppResponse(ResponseType type, object? contract)
        {
            ApiResponse response;

            response = new ApiResponse { data = contract };
            switch (type)
            {
                case ResponseType.Success:
                    response.status = 0;
                    response.message = "Success";

                    break;
                case ResponseType.NotFound:
                    response.status = 2;
                    response.message = "No record available";
                    break;
            }
            return response;
        }
    }
}
