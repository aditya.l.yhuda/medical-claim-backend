﻿namespace MedicalClaimAPI.DTOs.Response
{
    public class ApiResponse
    {
        public string message { get; set; }
        public int status { get; set; }

        public object? data { get; set; }

    }
}
