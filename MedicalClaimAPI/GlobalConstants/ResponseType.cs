﻿namespace MedicalClaimAPI.GlobalConstants
{
    public enum ResponseType
    {
        Success,
        NotFound,
        Failure
    }

    public enum StatusType
    {
        Pending,
        Approved, 
        Rejected
    }
}
