﻿using MedicalClaimAPI.Data;
using MedicalClaimAPI.Interfaces;
using MedicalClaimAPI.MappingProfiles;
using MedicalClaimAPI.Repositories;
using MedicalClaimAPI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

//Add Services to the Container.
builder.Services.AddAutoMapper(typeof(MedicalClaimProfile));
builder.Services.AddDbContext<MedicalClaimsDbContext>(
    options => options.UseNpgsql(builder.Configuration.GetConnectionString("MedicalClaimsDB")));
builder.Services.AddScoped<IMedicalClaimRepository, MedicalClaimRepository>();
builder.Services.AddScoped<IMedicalClaimService, MedicalClaimService>();

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowReactFrontend",
        builder =>
        {
            builder.WithOrigins("http://localhost:3000") // Replace with your frontend URL
                   .AllowAnyOrigin()
                   .AllowAnyHeader()
                   .AllowAnyMethod();
        });
});

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Register the Swagger generator, defining 1 or more Swagger documents
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "MedicalClaimsAPI", Version = "v1" });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Enable middleware to serve generated Swagger as a JSON endpoint.
//app.UseSwagger();

// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
// specifying the Swagger JSON endpoint.
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "MedicalClaimsAPI V1");
    c.RoutePrefix = string.Empty;  // Set Swagger UI at the app's root
});

app.UseHttpsRedirection();
app.UseAuthorization();
app.UseRouting();
app.UseCors("AllowReactFrontend");
app.MapControllers();

app.Run();
